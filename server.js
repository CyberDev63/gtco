"use strict";
//import dependencies
import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import morgan from "morgan"
import connectDB from "./config/db.js";

//Load Configuration
dotenv.config({path: "./config/.env"})

//Import routes
import {customer, discount, invoice} from "./routes/index.js"
import errorHandler from "./middleware/errorHandler.js";

const app = express();

//Connect to database
connectDB();

//Mount Middlewares
//Body Parser
app.use(express.json())
app.use(cors());

//Logger
if(process.env.NODE_ENV !== 'production'){
    app.use(morgan('dev'))
}

//Mount Route
app.use('/customers', customer);
app.use('/discounts', discount);
app.use('/invoices', invoice);
//Error Handler Route
app.use(errorHandler)

const server = app.listen(process.env.PORT, () => {
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${process.env.PORT}`); 
});

// Handle global unhandled promise rejections
process.on("unhandledRejection", (err, data) => {
  console.log(`Error: ${err.message}`);
  server.close(() => process.exit(1));
});
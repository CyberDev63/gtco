import { expect } from "chai";
import mongoose from "mongoose";
import { createCustomer, findCustomerById } from "../controller/customer.js";
import { createDiscount } from "../controller/discount.js";

describe("Customer Route Controller", () => {
  it("should throw an error if email and password is empty", (done) => {
    const req = {
      body: {
        email: "",
        password: "",
      },
    };
    createCustomer(req, {}, (err, response) => Promise.resolve(err)).then(
      (result) => {
        expect(result).to.be.an("error");
        expect(result).to.have.property("statusCode", 400);
        done();
      }
    );
  });

  it("should return an error with statusCode 500 if Id cant be cast or invalid ",  (done) => {
      const req = {
        params: {
          id: ''
        }
      };
    findCustomerById(req, {}, (err, response) => Promise.resolve(err))
    .then((result) => {
        expect(result).to.be.an('error')
        expect(result).to.have.property('statusCode', 500);
        done();
    })
    });
});

describe("Discount Route Controller", () => {
  it("should throw an error if type and percentage is empty", (done) => {
    const req = {
      body: {
        type: "",
        percentage: "",
      },
    };
    createDiscount(req, {}, (err, response) => Promise.resolve(err)).then(
      (result) => {
        expect(result).to.be.an("error");
        expect(result).to.have.property("statusCode", 400);
        done();
      }
    );
  });
});

"use strict";

import { asyncHandler } from "../middleware/asyncHandler.js";
import { Customer } from "../model/index.js";

import ErrorResponse from "../utils/ErrorResponse.js";

// @desc    Get a list of Customer

export const customerList = asyncHandler(async (req, res, next) => {
  const users = await Customer.find().select('+password').sort({ createdAt: -1 });

  if (!users) {
    return next(new ErrorResponse("No Customer Found", 404));
  }

  return res.status(200).json({
    success: true,
    users: users,
  });
});

// @desc    Create a Customer

export const createCustomer = asyncHandler(async (req, res, next) => {
  const { firstName, lastName, email, password, phone } = req.body;
  if (!email && !password) {
    return next(new ErrorResponse("Kindly provide a Email and Password", 400));
  }

  let check_email = await Customer.find({ email: email });
  if (check_email.length > 0) {
    return next(
      new ErrorResponse("Customer with the same email already exists", 400)
    );
  }
  
  // else create a new user

  const user = await Customer({
    name: firstName.concat(" ", lastName),
    email: email.toLowerCase(),
    password,
    phone,
  });
  await user.save();
  //Destructure Customer details to avoid sending password with user details
  //   const {pass, other} = user._doc;
  return res.status(201).json({
    success: true,
    user: user,
  });
});
// @desc    Get a specific customer by ID

export const findCustomerById = asyncHandler(async (req, res, next) => {
  const user = await Customer.findById(req.params.id);
  console.log(user);
  if (!user) {
    return next(new ErrorResponse("No Customer Found with this ID", 404));
  }
  res.status(200).json({
    success: true,
    user: user,
  });
});

// @desc    Get a specific customer by name

export const findCustomerByName = asyncHandler(async (req, res, next) => {
  const user = await Customer.findOne({ name: req.query.name });
  if (!user) {
    return next(new ErrorResponse("No Customer Found with this Name", 404));
  }
  res.status(200).json({
    success: true,
    user: user,
  });
});

import generateCustomerDiscount from "../utils/calculate.js";
import { Customer, Discount, Invoice } from "../model/index.js";
import uuid4 from "uuid4";
import { isRegOverTwoYrs } from "../utils/year.js";
import ErrorResponse from "../utils/ErrorResponse.js";
import { asyncHandler } from "../middleware/asyncHandler.js";

export const getTotal = asyncHandler(async (req, res, next) => {
  let customer = req.body;
  const retrieveDiscount = await Discount.findOne({
    type: customer.membership_type,
  });

  let totalPrice = 0;
  //Calculate Customer Product Price with Quantity and Store it as Bill in Customer Object
  customer.products.forEach((prod) => {
    totalPrice += prod.quantity * prod.price;
  });

  customer.bill = totalPrice;
  customer.percentage = retrieveDiscount.percentage;
  //check customer membership type and if use check it has been in the store over two years
  if (customer.membership_type === "customer") {
    const user = await Customer.findOne({ email: customer.email });
    const isCustomerOverTwoYears = isRegOverTwoYrs(user.createdAt);
    //if user is not a customer for two years
    if (!isCustomerOverTwoYears) {
      const invoice = new Invoice({
        customer_id: customer.customer_id,
        amount_paid: customer.bill,
        discount_amount: 0,
        product_item: customer.products,
        receipt_number: uuid4(),
      });
      invoice.save();
      return res.json({
        success: true,
        invoice: {
          percentage: 0,
          amount: customer.bill,
          amount_to_pay_after_discount: customer.bill,
        },
      });
    }
  }

  const discount = generateCustomerDiscount(customer);
  //Create invoice
  const invoice = new Invoice({
    customer_id: customer._id,
    amount_paid: discount.amount,
    discount_amount:
      discount.amount_to_pay_after_discount +
      discount.discount_on_bill_higher_than_100,
    discount_type: retrieveDiscount._id,
    product_item: customer.products,
    receipt_number: uuid4(),
  });
  invoice.save();
  res.json({
    success: true,
    invoice: discount,
  });
});

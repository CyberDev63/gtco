"use strict";

import { asyncHandler } from "../middleware/asyncHandler.js";
import { Discount } from "../model/index.js";

import ErrorResponse from "../utils/ErrorResponse.js";

// @desc    Get a list of all discounts

export const discountList = asyncHandler(async (req, res, next) => {
  const discounts = await Discount.find().sort({ createdAt: -1 });

  if (!discounts) {
    return next(new ErrorResponse("No Discount Found", 404));
  }
  return res.status(200).json({
    success: true,
    discounts: discounts,
  });
});

// @desc    Get a specific discount percentage by type

export const findDiscountByType = asyncHandler(async (req, res, next) => {
  const discount = await Discount.findOne({ type: req.query.type });
  if (!discount) {
    return next(new ErrorResponse("No Discount Found with this type", 404));
  }
  const { type, percentage } = discount;
  res.status(200).json({
    success: true,
    type: type,
    percentage: percentage,
  });
});

// @desc    Add a new discount type

export const createDiscount = asyncHandler(async (req, res, next) => {
  const { type, percentage } = req.body;

  if (!type && !percentage) {
    return next(new ErrorResponse("Kindly provide a type and percentage", 400));
  }

  let check_type = await Discount.find({ type: type });
  if (check_type.length > 0) {
    return next(
      new ErrorResponse("Discount with the same type already exists", 400)
    );
  }
  // else create a new user

  const discount = await Discount({
    type,
    percentage,
  });
  await discount.save();

  return res.status(201).json({
    success: true,
    discount: discount,
  });
});

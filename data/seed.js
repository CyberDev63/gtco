import mongoose from "mongoose";
import { Customer } from "../model/index.js";
import { Discount } from "../model/index.js";
const DB_URL =
  "mongodb+srv://sales:4ZFlqRtGjyiGqC7S@cluster0.i0pqxcn.mongodb.net/api_db?retryWrites=true&w=majority";

await mongoose.connect(DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const customers = [
  {
    name: "Cris Phythean",
    email: "cphythean0@bbb.org",
    password: "gt9R6Nz7g",
    phone: "241-954-7155",
  },
  {
    name: "Lanna Shelper",
    email: "lshelper1@latimes.com",
    password: "rlhPrY",
    phone: "420-811-0174",
  },
  {
    name: "Ted MacRorie",
    email: "tmacrorie2@cpanel.net",
    password: "kt8JHrA86m",
    phone: "283-139-5916",
  },
  {
    name: "King Kornousek",
    email: "kkornousek3@hao123.com",
    password: "7977je",
    phone: "369-129-1544",
  },
  {
    name: "Craig Penwright",
    email: "cpenwright4@irs.gov",
    password: "BO3FvDW",
    phone: "415-739-7375",
  },
  {
    name: "Liz Glaysher",
    email: "lglaysher5@jigsy.com",
    password: "iZgICLO",
    phone: "729-433-0507",
  },
  {
    name: "Francisco Fosh",
    email: "ffosh6@networkadvertising.org",
    password: "2C5IQYt",
    phone: "116-311-6162",
  },
  {
    name: "Ricca Mossom",
    email: "rmossom7@myspace.com",
    password: "tfGUMV9tLzjK",
    phone: "738-852-7087",
  },
  {
    name: "Brennan Sandbrook",
    email: "bsandbrook8@sfgate.com",
    password: "Ca1j6V7MOAM",
    phone: "394-751-5216",
  },
  {
    name: "Lyle Swyndley",
    email: "lswyndley9@1688.com",
    password: "MSXtRkjBqj",
    phone: "182-391-6600",
  },
];

const discounts = [
  {
    type: "customer",
    percentage: 5,
  },
  {
    type: "affiliate",
    percentage: 10,
  },
  {
    type: "employee",
    percentage: 30,
  },
];

const seedDB = async () => {
  //clear existing data from the database [Customer and Discount]
  await Customer.deleteMany();
  await Discount.deleteMany();
  //insert seed data to the database [Customer and Discount]
  await Customer.insertMany(customers);
  await Discount.insertMany(discounts);

  console.log("Data Seeding Successfull....");
};

//Close DB Connection
seedDB().then(() => {
  mongoose.connection.close();
  console.log("Database Connection Closed....");
});

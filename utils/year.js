export const isRegOverTwoYrs = (reg) => {
    //get full year from the date of user registration
    const createdAt = new Date(reg).getFullYear()
    const now = new Date;
    //subtract user year of registration from date they are making the purchase
    if(now.getFullYear() - createdAt > 2){
       return true
    }
    return false;
}
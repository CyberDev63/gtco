const generateCustomerDiscount = (user) => {
  //destructure user to get necessary information
  const { percentage, bill } = user;
  //calculate discount based on memebership type ['employee', 'affiliate', 'customer']
  const genDisc = generateDiscountMembershipType(percentage, bill);
  //calculate discount based on bill over $100
  const discBill = generateDiscountOnBill(bill);

  let total = 0;
  const totalDiscount = genDisc + discBill;
  total = bill - totalDiscount;
    return {
      percentage,
      amount: bill,
      discount_on_bill_higher_than_100: discBill,
      amount_to_pay_after_discount: total,
    };

};

// Discount calculation on purchase product based on memebership type
const generateDiscountMembershipType = (percentage, bill) => {
  const discount = (percentage / 100) * bill;
  return discount;
};

// Discount calculation on Bills over $100
const generateDiscountOnBill = (bill) => {
  if (bill > 99) {
    const billDiscount = bill / 100;
    let value = getIntegerPart(billDiscount);
    return value * 5;
  }
  return 0;
};
//Convert bill by 100 to get discount amount
const getIntegerPart = (num) => {
  if (Number.isInteger(num)) {
    return num;
  }

  const decimalString = num.toString().split(".")[0];
  return Number(decimalString);
};

export default generateCustomerDiscount;

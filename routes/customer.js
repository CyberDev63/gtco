"use strict";
import {Router} from 'express'
import { createCustomer, customerList, findCustomerById, findCustomerByName } from '../controller/customer.js';

const router = Router();



// @desc    Get a list of Customer
// @route   GET /customers

router.get('/', customerList)

// @desc    Create a customer
// @route   POST /customers/create

router.post('/create', createCustomer)

// @desc    Get a specific customer by ID
// @route   GET /customers/id

router.get('/:id', findCustomerById)

// @desc    Get a specific customer by name
// @route   GET /customers/name

router.get('/find/name', findCustomerByName)

export { router}
export {router as customer} from './customer.js';
export {router as discount} from './discount.js';
export {router as invoice} from './invoice.js';
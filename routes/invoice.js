"use strict";
import {Router} from 'express'

import {getTotal} from '../controller/invoices.js'

const router = Router();

// @desc    Get a list of all discounts
// @route   GET /discounts

router.get('/', getTotal)


export {router}
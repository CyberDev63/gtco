"use strict";
import {Router} from 'express'
import { createDiscount, discountList, findDiscountByType } from '../controller/discount.js';

const router = Router();

// @desc    Get a list of all discounts
// @route   GET /discounts

router.get('/', discountList)

// @desc    Get a specific discount percentage by type
// @route   GET /discounts/type

router.get('/type', findDiscountByType)

// @desc    Add a new discount type
// @route   POST /discounts/create

router.post('/create', createDiscount)

export {router}
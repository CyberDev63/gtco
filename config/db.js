import mongoose from "mongoose";
export default  async() => {
  const db = mongoose.connection;
  try {
    db.on("connected", function() {
      console.log(`DB connected successfully on ${db.host}`);
    });
    db.on("disconnected", function() {
      console.log("Mongoose disconnected");
    });
    db.on("error", function(err) {
      console.log(`Error occurred ${err}`);
    });

    return await mongoose.connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
  } catch (error) {
    console.log(`Error occurred ${error}`);
  }
};

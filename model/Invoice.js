import mongoose from "mongoose";

const Schema = mongoose.Schema;

const invoiceSchema = new Schema({
  customer_id: {
    type: mongoose.Types.ObjectId,
    ref: "customer",
  },
  amount_paid: {
    type: Number,
  },
  currency: {
    type: String,
    default: "USD",
  },
  discount_type: {
    type: mongoose.Types.ObjectId,
    ref: "discount",
    default: null
  },
  discount_amount: {
    type: Number,
  },
  product_item: {
    type: Array,
  },
  // [
  //   {
  //     name: {
  //       type: String,
  //       required: true,
  //     },
  //     quantity: {
  //       type: String,
  //     },
  //     amount: {
  //       type: Number,
  //     },
  //   },
  // ],
  receipt_number: {
    type: String,
  },
  payment_method: {
    type: String,
    default: "MasterCard",
  }
}, {
    timestamps: true
});

const invoice = mongoose.model('invoice', invoiceSchema)

export {invoice}
"use strict";
import mongoose from "mongoose";

const Schema = mongoose.Schema;

const discountSchema = new Schema(
  {
    type: {
      type: String,
      required: true
    },
    percentage: {
      type: Number,
      required: true,
    },
    valid_until: {
      type: Date
    },
    isFreeShipping: {
        type: Boolean,
        default: false
    }
},
  {
    timestamps: true,
  }
);

const discount = mongoose.model('Discount', discountSchema);

export {discount}

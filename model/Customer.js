"use strict";
import mongoose from "mongoose";

import bcrypt from "bcryptjs";

const Schema = mongoose.Schema;

const customerSchema = new Schema(
  {
    membership_type: {
      type: String,
      enum: ["employee", "affilate", "customer"],
      default: "customer",
    },
    name: {
      type: String,
      trim: true,
    },
    email: {
      type: String,
      trim: true,
      required: true,
    },
    password: {
      type: String,
      trim: true,
      required: [true, "Please provide a password"],
      select: false,
    },
    gender: { 
      type: String, 
      default: "male" 
    },
    dob: {
      type: String,
      trim: true,
    },
    balance: {
      type: Number,
    },
    phone: {
      type: String,
      unique: true,
      required: [true, "Please provide a phone number"],
      // match: [/^0[789][01][0-9]{8}$/, "Please provide a valid phone number"]
    },
    address: { type: String, default: "" },
    country: { type: String, default: "" },
    state: { type: String, default: "" },
    city: { type: String, default: "" },
    about: { type: String, default: "" },
    zipCode: { type: String, default: "" },

  },
  {
    toJSON:{
      transform(doc, ret){
        delete ret.__v;
        delete ret.createdAt;
        delete ret.updatedAt;
      }
    }
  },
  {
    timestamps: true,
  }
);

customerSchema.pre("save", async function(next) {
  if (!this.isModified("password")) {
    next();
  }
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

const customer = mongoose.model('Customer', customerSchema);

export {customer}
